package lsg.consumables.drinks;

public class SmallStamPotion extends Drink{
    public SmallStamPotion() {
        super("Small Stam Potion", 20);
    }
}
