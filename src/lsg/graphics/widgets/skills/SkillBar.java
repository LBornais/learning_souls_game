package lsg.graphics.widgets.skills;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

import java.util.LinkedHashMap;
import java.util.Map;

public class SkillBar extends HBox {
    private static LinkedHashMap<KeyCode, String> DEFAULT_BINDING = new LinkedHashMap<>();

    private ConsumableTrigger consumableTrigger = new ConsumableTrigger(KeyCode.C, "C", null, null);

    static {
        DEFAULT_BINDING.put(KeyCode.DIGIT1, "&");
        DEFAULT_BINDING.put(KeyCode.DIGIT2, "é");
        DEFAULT_BINDING.put(KeyCode.DIGIT3, "\"");
        DEFAULT_BINDING.put(KeyCode.DIGIT4, "'");
        DEFAULT_BINDING.put(KeyCode.DIGIT5, "(");
    }

    private SkillTrigger triggers[];

    public SkillBar() {
        this.setSpacing(10.0);
        this.prefHeight(110);
        this.setAlignment(Pos.CENTER);
        init();
    }

    private void init(){
        triggers = new SkillTrigger[DEFAULT_BINDING.size()];
        int i =0;
        for (Map.Entry<KeyCode, String> binding : DEFAULT_BINDING.entrySet()){
            triggers[i] = new SkillTrigger(binding.getKey(), binding.getValue(), null, null);

            this.getChildren().add(triggers[i]);
            i++;
        }
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(30);
        this.getChildren().add(rectangle);
        this.getChildren().add(consumableTrigger);
    }

    public SkillTrigger getTrigger(int slot){
        return triggers[slot];
    }

    public ConsumableTrigger getConsumableTrigger() {
        return consumableTrigger;
    }

    public void process(KeyCode code){
        if (!this.isDisabled()){
            if (consumableTrigger.getKeyCode().equals(code)){
                consumableTrigger.trigger();
            }else{
                for (int i = 0; i< triggers.length; i++){
                    if (getTrigger(i).getKeyCode().equals(code)){
                        getTrigger(i).trigger();
                        break;
                    }
                }
            }
        }
    }
}
